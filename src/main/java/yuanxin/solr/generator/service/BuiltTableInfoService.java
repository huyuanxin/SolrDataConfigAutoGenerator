package yuanxin.solr.generator.service;


import com.baomidou.mybatisplus.extension.service.IService;
import yuanxin.solr.generator.entity.BuiltTableInfo;


/**
 * @author huyuanxin
 */
public interface BuiltTableInfoService extends IService<BuiltTableInfo> {


}

